import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AddQuestionPage } from '../add-question/add-question';
import {ViewMcqPage} from '../view-mcq/view-mcq'
import { QuizProvider } from '../../providers/quiz/quiz';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  categories = [];
  booleanTags = false;
  constructor(public navCtrl: NavController, public quizService: QuizProvider) {

  }
  ionViewDidLoad(){
    this.quizService.getTopics().then(res =>{
      // console.log(res)
      for(var i in res){
        this.categories.push(res[i].name)
      }
      if(this.categories.length > 0){
        this.booleanTags = true
      }
      console.log(this.categories)
    },err => {
      console.log(err)
    })
  }
  goToAddQuestionPage(){
    this.navCtrl.push(AddQuestionPage)
  }

  goToTopic(i){  
    this.navCtrl.push(ViewMcqPage,{topic:this.categories[i]})
  }
}
