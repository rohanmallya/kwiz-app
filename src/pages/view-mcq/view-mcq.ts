import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuizProvider } from '../../providers/quiz/quiz';

/**
 * Generated class for the ViewMcqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-mcq',
  templateUrl: 'view-mcq.html',
})
export class ViewMcqPage {
  topic:any;
  questions = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public quizService:QuizProvider) {
    this.topic = this.navParams.get('topic')
  }

  class(c){
    if(c == true){
      return "right"
    }
    return "wrong"
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewMcqPage');
    this.quizService.getQuestions(this.topic).then(res => {
      for(var i in res){
        this.questions.push(res[i])
      }
    }, err => {
      console.log(err)

    })
  }

}
