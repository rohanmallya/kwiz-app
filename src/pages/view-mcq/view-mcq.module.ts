import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewMcqPage } from './view-mcq';

@NgModule({
  declarations: [
    ViewMcqPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewMcqPage),
  ],
})
export class ViewMcqPageModule {}
