import {
  Component
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from 'ionic-angular';
import {
  FormBuilder,
  FormArray,
  FormGroup,
  Validators
} from '@angular/forms';
import { QuizProvider } from '../../providers/quiz/quiz';


/**
 * Generated class for the AddQuestionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-question',
  templateUrl: 'add-question.html',
})
export class AddQuestionPage {

  public form: FormGroup;
  tags = [];
  booleanTags = false;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private _FB: FormBuilder,
    public alertCtrl: AlertController,
    public quizService: QuizProvider) {

    // Define the FormGroup object for the form
    // (with sub-FormGroup objects for handling
    // the dynamically generated form input fields)
    this.form = this._FB.group({
      question: ['', Validators.required],
      answers: this._FB.array([
        this.initFields(),
        this.initFields(),
        this.initFields(),
        this.initFields()
      ])
    });
  }


  initFields(): FormGroup {
    return this._FB.group({
      answer: ['', Validators.required],
      correct: [false, Validators.required]
    });
  }


  addNewInputField(): void {
    const control = < FormArray > this.form.controls.options;
    control.push(this.initFields());
  }

  removeInputField(i: number): void {
    const control = < FormArray > this.form.controls.options;
    control.removeAt(i);
  }


  makeTagsList() {
    let prompt = this.alertCtrl.create({
      title: 'Add tag',
      inputs: [{
        name: 'tag',
        placeholder: "Add tag"
      }],
      buttons: [{
          text: 'Cancel'
        },
        {
          text: 'Add',
          handler: data => {
            // console.log("here^");
            this.tags.push(data['tag']);
          }
        }
      ]
    });

    prompt.present();
    this.booleanTags = true;
    console.log(this.tags);
  }

  deleteTag(i: number) {
    this.tags.splice(i, 1);
    console.log(this.tags);
  }

  manage(val: any): void {
    val.answers[0].correct = true
    // console.dir(val);

    var JSON = {
      "question":val.question,
      "answers":val.answers,
      "categories":this.tags
    }

    this.quizService.createMCQ(JSON).then(res => {
      console.log(res);
      // this.navCtrl.setRoot()
    }, err => {
      console.log(err)
    })

    console.log("HERE")
  }
}
