import {
  Injectable
} from '@angular/core';
import {
  Http,
  Headers
} from '@angular/http';
import {
  Storage
} from '@ionic/storage';
import 'rxjs/add/operator/map';

import {
  CONFIG
} from '../../../config/config';

import {
  AuthProvider
} from '../auth/auth';

@Injectable()
export class QuizProvider {

  public token: any;
  public API_URL = CONFIG['API_URL'];
  constructor(public http: Http, public storage: Storage, public authService: AuthProvider) {
    this.token = authService.token
  }

  createMCQ(details) {

    return new Promise((resolve, reject) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', this.token);
      this.http.post(this.API_URL + '/mcq/add', JSON.stringify(details), {
          headers: headers
        })
        .subscribe(res => {
          let data = res.json();
          resolve(data);

        }, (err) => {
          reject(err);
        });

    });

  }

  getTopics(){
    return new Promise((resolve, reject) => {

      let headers = new Headers();
      // headers.append('Content-Type', 'application/json');
      headers.append('Authorization', this.token);
      this.http.get(this.API_URL + '/mcq/topics',  {
          headers: headers
        })
        .subscribe(res => {
          let data = res.json();
          resolve(data);

        }, (err) => {
          reject(err);
        });

    });
  }

  getQuestions(details){
    return new Promise((resolve, reject) => {
      var topic = (details.split(" ")).join("+")
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', this.token);
      this.http.get(this.API_URL + '/mcq/'+topic+'/get', {
          headers: headers
        })
        .subscribe(res => {
          let data = res.json();
          resolve(data);

        }, (err) => {
          reject(err);
        });

    });
  }
}
